import Foundation

class SecretMenuData {
    private let userDefaults = UserDefaults.standard

    var useStagingApi: Bool {
        get {
            return userDefaults.bool(forKey: "useStagingApi")
        }
        set {
            userDefaults.set(newValue, forKey: "useStagingApi")
        }
    }

    var showTopics: Bool {
        get {
            return userDefaults.bool(forKey: "showTopics")
        }
        set {
            userDefaults.set(newValue, forKey: "showTopics")
        }
    }

    init () {
        // remove by September 2016
        userDefaults.removeObject(forKey: "showCreateCommunity")
    }
}
