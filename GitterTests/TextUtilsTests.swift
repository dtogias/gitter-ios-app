import Foundation
import XCTest
@testable import Gitter

class TextUtilsTests: XCTestCase {

    func testEmptyString() {
        let result = TextUtils.getClosestWord("", underCursor: NSRange(location: 0, length: 0))

        XCTAssertNil(result)
    }

    func testSingleWord() {
        let result = TextUtils.getClosestWord("butterfly", underCursor: NSRange(location: 3, length: 0))!

        assertEqual(result, (word: "butterfly", range: NSRange(location: 0, length: 9)))
    }

    func testWordPrefixedByWhitespace() {
        let result = TextUtils.getClosestWord(" butterfly", underCursor: NSRange(location: 3, length: 0))!

        assertEqual(result, (word: "butterfly", range: NSRange(location: 1, length: 9)))
    }

    func testWordSuffixedByWhitespace() {
        let result = TextUtils.getClosestWord("butterfly", underCursor: NSRange(location: 3, length: 0))!

        assertEqual(result, (word: "butterfly", range: NSRange(location: 0, length: 9)))
    }

    func testWordSurroundedByWhitespace() {
        let result = TextUtils.getClosestWord(" butterfly ", underCursor: NSRange(location: 3, length: 0))!

        assertEqual(result, (word: "butterfly", range: NSRange(location: 1, length: 9)))
    }

    func testEmoji() {
        let result = TextUtils.getClosestWord("🐯", underCursor: NSRange(location: 0, length: 0))!

        XCTAssertEqual(result.word, "🐯")
        // surprise! emoji count as two utf symbols in NS land
        assertEqual(result, (word: "🐯", range: NSRange(location: 0, length: 2)))
    }

    func testEmojiBeforeWord() {
        let result = TextUtils.getClosestWord("🐯 roar", underCursor: NSRange(location: 5, length: 0))!

        assertEqual(result, (word: "roar", range: NSRange(location: 3, length: 4)))
    }

    func testHighlightedTwoWords() {
        let result = TextUtils.getClosestWord("cat dog", underCursor: NSRange(location: 0, length: 7))

        XCTAssertNil(result)
    }

    func assertEqual(_ a: (word: String, range: NSRange)?, _ b: (word: String, range: NSRange)?) {
        XCTAssertEqual(a?.word, b?.word)
        XCTAssertEqual(a?.range.location, b?.range.location)
        XCTAssertEqual(a?.range.length, b?.range.length)
    }
}
